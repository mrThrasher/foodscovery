<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//add to cart
Route::post('/add_cart', 'CartController@addProduct')->middleware('checkProductAddToCart');

//remove to cart
Route::post('/rm_cart', 'CartController@rmProduct');

Route::post('/checkout', 'CartController@checkout')->name('checkout');

