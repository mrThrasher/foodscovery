<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Food\Product;
use App\Food\Cart;
use Auth;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        /*
        Eseguo i calcoli per calcolare le tasse , il badge con i prodotti e gestisco i prodotti nel carrello 
        */
        $userId = Auth::user()->id;
        $numProdottiCarrello = Cart::where('user_id',$userId)->count();

        $prodotti = Product::all();
        $products = array();
        $prodottiCarrello = array();


        $prodottiWithotTax = DB::table('products')->join('carts','products.id','=','carts.products_ids')->where('carts.user_id',$userId)->get();
        foreach ($prodottiWithotTax as $key => $value) {
            $prodottiIva = ($value->taxable_price * $value->rate) / 100;
            $prodottiCarrello[] = array('id_prodotto' =>$value->products_ids, 'nome' => $value->name ,'prezzo' => $value->taxable_price + $prodottiIva);
        }

        foreach ($prodotti as $key => $value) {
            $prodottiIva = ($value->taxable_price * $value->rate) / 100;
            $products[] = array(
                'id_prodotto' =>$value->id,
                'nome' => $value->name,
                'prezzo' => $value->taxable_price + $prodottiIva
            );
        }

        return view('home', compact('products','numProdottiCarrello','prodottiCarrello'));
    }
}


