<?php

namespace App\Food;

/**
 * Class Product
 * @package App\Food
 *
 * modello per la tabella dei prodotti
 */
class Product extends Base
{
    protected $fillable = [
        'name', 'taxable_price', 'rate',
    ];
}
