<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Food\Cart;
use Auth;
use App\Food\Product;

class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function addProduct(Request $request)
    {

       $addToCart = new Cart();
       $addToCart->user_id = $request->id_user;
       $addToCart->products_ids = $request->id_product;
       $addToCart->save();
       return redirect()->back()->with('success', 'Prodotto caricato nel carrello.');   
    }
    
    public function rmProduct(Request $request)
    {
        Cart::where('products_ids',$request->id_cart)->where('user_id',$request->id_user)->delete();
        return redirect()->back()->with('success', 'Prodotto rimosso dal carrello.');
    }

    public function checkout(Request $request){
        /*
        Calcolo il totale del carrello tasse incluse
        */
        $prodottoArray = array();
        $sommaPrezzoIvato = 0;
        $carrello = Cart::where('user_id',$request->id_user)->get();
        foreach ($carrello as $value) {
            $prodotto = Product::where('id',$value->products_ids)->get();
            $prodottoArray[] = array('carrello'=>$prodotto);
        }

        foreach ($prodottoArray as $key => $value) {
            $prezzoSenzaTasse = $value['carrello'][0]->taxable_price;
            $percentuale = $value['carrello'][0]->rate;
            $prezzoIvato = ($prezzoSenzaTasse * $percentuale) / 100;
            $sommaPrezzoIvato += $prezzoIvato;
        }
        return view('checkout')->with('totale', $sommaPrezzoIvato);
     }
}
