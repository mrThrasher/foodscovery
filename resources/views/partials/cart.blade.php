<div class="container">
   <table class="table">
     <thead>
       <tr>
         <th scope="col">Prodotto</th>
         <th scope="col">Prezzo</th>
       </tr>
     </thead>
     <tbody>
      @foreach($prodottiCarrello as $prodotto)
         <tr>
            <td>{{$prodotto['nome']}}</td>
            <td>{{$prodotto['prezzo']}}</td>
         </tr>
      @endforeach
    </tbody>
   </table>
    <hr>
    <form method="post" action="{{url('/checkout')}}">
      {{ csrf_field() }}
      <input type="hidden" name="id_user" value="{{Auth::id()}}">
      <input class="btn btn-primary" type="submit" value="checkout">
    </form>
</div>