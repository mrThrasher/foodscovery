<?php

namespace App\Http\Middleware;
use App\Food\Cart;
use Closure;

class checkProductAddToCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cart = Cart::all();
        foreach ($cart as $value) {
            if ($request->id_product == $value->products_ids){
                return redirect()->back()->with('error', 'Prodotto già caricato nel carrello.');
            }
        }
        return $next($request);
    }
}
