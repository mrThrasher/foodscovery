@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-8 offset-md-2">
            <div class="container">
                @include('partials.errors')
                <h2>Prodotti</h2>         
                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Nome Prodotto</th>
                        <th>Prezzo Ivato</th>
                        <th>Azioni</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $prodotto)
                        <tr>
                            <td>{{$prodotto['nome']}}</td>
                            <td>{{$prodotto['prezzo']}}</td>
                            <th>
                                    <form method="post" action="{{url('/add_cart')}}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id_user" value="{{Auth::id()}}">
                                        <input type="hidden" name="id_product" value="{{$prodotto['id_prodotto']}}">
                                        <input class="btn btn-primary" type="submit" value="add">
                                    </form>
                                @foreach($prodottiCarrello as $cartProd)
                                    @if($cartProd['id_prodotto'] == $prodotto['id_prodotto'])
                                        <form method="post" action="{{url('/rm_cart')}}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id_user" value="{{Auth::id()}}">
                                            <input type="hidden" name="id_cart" value="{{$cartProd['id_prodotto']}}">
                                            <input class="btn btn-danger" type="submit" value="remove">
                                        </form>
                                    @endif
                                @endforeach
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
