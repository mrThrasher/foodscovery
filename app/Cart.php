<?php

namespace App\Food;

/**
 * Class Cart
 * @package App\Food
 *
 * 
 */
use Illuminate\Database\Eloquent\Model;

class Cart extends Base
{	
	protected $connection = 'food';
    protected $fillable = ['user_id','products_ids'];
}
